//
//  ShowNameAndSurnameViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class ShowNameAndSurnameViewController: UIViewController {
    @IBOutlet weak var helloLabel: UILabel!
    private var name: String?
    private var surname: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        updateHelloLabel()
        // Do any additional setup after loading the view.
    }
    
    func updateHelloLabel() {
        if let name = name, let surname = surname {
            helloLabel.text = "Hello, \(name) \(surname)!"
        }
    }
    
    func setViewController(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
