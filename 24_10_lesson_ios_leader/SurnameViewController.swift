//
//  SurnameViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

protocol SurnameViewControllerDelegate: class {
    func updateNameAndSurname(name: String, surname: String)
}

class SurnameViewController: UIViewController {
    weak var delegate: SurnameViewControllerDelegate?
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    private var name: String = ""
    private var surname: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.isEnabled = false
        
        if name.count > 0 {
            surnameTextField.text = surname
            nextButton.isEnabled = true
        }
        // Do any additional setup after loading the view.
    }
    
    func setViewController(name: String, surname: String, sender: SurnameViewControllerDelegate) {
        self.name = name
        self.surname = surname
        delegate = sender
    }

    @IBAction func surnameTextFieldChanged(_ sender: UITextField) {
        surname = sender.text ?? ""
        if surname.count > 0 {
            nextButton.isEnabled = true
        }
        else {
            nextButton.isEnabled = false
        }
    }
    @IBAction func nextButtonTouched(_ sender: Any) {
        delegate?.updateNameAndSurname(name: name, surname: surname)
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
