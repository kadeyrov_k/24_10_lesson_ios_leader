//
//  SetNameAndSurnameViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit
//ShowNameAndSurnameVC
class SetNameAndSurnameViewController: UIViewController {
    private var name: String?
    private var surname: String?
    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func nameEditingChange(_ sender: UITextField) {
        name = sender.text
        updateNextButton()
    }
    
    @IBAction func surnameEditingChange(_ sender: UITextField) {
        surname = sender.text
        updateNextButton()
    }
    
    func updateNextButton() {
        if let name = name, let surname = surname  {
            if name != "" && surname != "" {
                nextButton.isEnabled = true
            }
            else {
                nextButton.isEnabled = false
            }
        }
        else {
            nextButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowNameAndSurnameVC" {
            if let showNameAndSurnameVC = segue.destination as? ShowNameAndSurnameViewController {
                if let name = name, let surname = surname {
                    showNameAndSurnameVC.setViewController(name: name, surname: surname)
                }
            }
        }
    }
    @IBAction func nextButtonTouch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let showNameAndSurnameVC = storyboard.instantiateViewController(identifier: "ShowNameAndSurnameVC") as? ShowNameAndSurnameViewController {
            if let name = name, let surname = surname {
                showNameAndSurnameVC.setViewController(name: name, surname: surname)
                present(showNameAndSurnameVC, animated: true, completion: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
