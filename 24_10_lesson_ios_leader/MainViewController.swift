//
//  MainViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    private var name: String?
    private var surname: String?
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var setOrChangeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateHelloLabelAndButton()
    }
    
    func updateHelloLabelAndButton() {
        if let name = name, let surname = surname {
            if name == "" && surname == "" {
                helloLabel.text = "Hello!"
                setOrChangeButton.setTitle("Set name and surname", for: .normal)
            }
            else {
                helloLabel.text = "Hello, \(name) \(surname)!"
                setOrChangeButton.setTitle("Change name and surname", for: .normal)
            }
        }
        else {
            helloLabel.text = "Hello!"
            setOrChangeButton.setTitle("Set name and surname", for: .normal)
        }
    }
    
    @IBAction func setOrChangeButtonTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let nameVC = storyboard.instantiateViewController(identifier: "NameVC") as? NameViewController  {
            nameVC.setViewController(name: (name ?? ""), surname: (surname ?? ""), sender: self)
            self.navigationController?.pushViewController(nameVC, animated: true)
        }
    }
}
extension MainViewController: SurnameViewControllerDelegate {
    func updateNameAndSurname(name: String, surname: String) {
        self.name = name
        self.surname = surname
        updateHelloLabelAndButton()
    }
    
    
}
