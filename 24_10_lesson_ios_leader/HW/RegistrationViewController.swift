//
//  RegistrationViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 31.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

protocol RegistrationViewControllerDelegate: class {
    func update(name: String, surname: String, phone: String)
}

class RegistrationViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var enterOrChangeButton: UIButton!
    
    weak var delegate: RegistrationViewControllerDelegate?
    
    private var name: String = ""
    private var surname: String = ""
    private var phone: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    func setView() {
        if name != "" && surname != "" && phone != "" {
            enterOrChangeButton.setTitle("Change", for: .normal)
            enterOrChangeButton.isEnabled = true
            nameTextField.text = name
            surnameTextField.text = surname
            phoneTextField.text = phone
        }
        else {
            enterOrChangeButton.setTitle("Enter", for: .normal)
            enterOrChangeButton.isEnabled = false
        }
    }
    func setViewContorller(name: String, surname: String, phone: String) {
        self.name = name
        self.surname = surname
        self.phone = phone
    }
    @IBAction func nameTextFieldChange(_ sender: UITextField) {
        if let name = sender.text {
            self.name = name
        }
        cheakButtonIsEnabled()
    }
    @IBAction func surnameTextFieldChange(_ sender: UITextField) {
        if let surname = sender.text {
            self.surname = surname
        }
        cheakButtonIsEnabled()
    }
    @IBAction func phoneTextFieldChange(_ sender: UITextField) {
        if let phone = sender.text {
            self.phone = phone
        }
        cheakButtonIsEnabled()
    }
    func cheakButtonIsEnabled() {
        if name != "" && surname != "" && phone != "" {
            enterOrChangeButton.isEnabled = true
        } else {
            enterOrChangeButton.isEnabled = false
        }
    }
    @IBAction func enterOrChangeButoonTouch(_ sender: Any) {
        delegate?.update(name: name, surname: surname, phone: phone)
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
