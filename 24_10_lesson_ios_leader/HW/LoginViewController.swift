//
//  LoginViewController.swift
//  24_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 31.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var registrationOrChangeButton: UIButton!
    @IBOutlet weak var helloLabel: UILabel!
    private var name: String = ""
    private var surname: String = ""
    private var phone: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        changeButtonTitle()
        if name != "" && surname != "" {
            helloLabel.text = "Hello, \(name) \(surname)"
        } else {
            helloLabel.text = "Hello"
        }
    }
    
    func changeButtonTitle() {
        if name == "" && surname == "" && phone == "" {
            registrationOrChangeButton.setTitle("Registration", for: .normal)
        }
        else {
            registrationOrChangeButton.setTitle("Change", for: .normal)
        }
    }
    
    @IBAction func registrationOrChangeButtonTouch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let registrationVC = storyboard.instantiateViewController(withIdentifier: "RegistrationVC") as? RegistrationViewController {
            registrationVC.setViewContorller(name: name, surname: surname, phone: phone)
            registrationVC.delegate = self
            self.navigationController?.pushViewController(registrationVC, animated: true)
        }
    }

}
extension LoginViewController: RegistrationViewControllerDelegate {
    func update(name: String, surname: String, phone: String) {
        self.name = name
        self.surname = surname
        self.phone = phone
    }
}
